import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.dynamicframe import DynamicFrame
from operator import add
from functools import reduce
from pyspark.sql import functions as F
 
## @params: [JOB_NAME]
args = getResolvedOptions(sys.argv, ['JOB_NAME'])
 
sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)
## @type: DataSource
## @args: [database = "consumption_db", table_name = "consumption_input", transformation_ctx = "datasource0"]
## @return: datasource0
## @inputs: []
datasource0 = glueContext.create_dynamic_frame.from_catalog(database = "consumption_db", table_name = "consumption_input", transformation_ctx = "datasource0")
## @type: ApplyMapping
## @args: [mapping = [("meter", "string", "meter", "string"), ("date", "string", "date", "string"), ("0:00", "long", "0:00", "long"), ("0:30", "long", "0:30", "long"), ("1:00", "long", "1:00", "long"), ("1:30", "long", "1:30", "long"), ("2:00", "long", "2:00", "long"), ("2:30", "long", "2:30", "long"), ("3:00", "long", "3:00", "long"), ("3:30", "long", "3:30", "long"), ("4:00", "long", "4:00", "long"), ("4:30", "long", "4:30", "long"), ("5:00", "long", "5:00", "long"), ("5:30", "long", "5:30", "long"), ("6:00", "long", "6:00", "long"), ("6:30", "long", "6:30", "long"), ("7:00", "long", "7:00", "long"), ("7:30", "long", "7:30", "long"), ("8:00", "long", "8:00", "long"), ("8:30", "long", "8:30", "long"), ("9:00", "long", "9:00", "long"), ("9:30", "long", "9:30", "long"), ("10:00", "long", "10:00", "long"), ("10:30", "long", "10:30", "long"), ("11:00", "long", "11:00", "long"), ("11:30", "long", "11:30", "long"), ("12:00", "long", "12:00", "long"), ("12:30", "long", "12:30", "long"), ("13:00", "long", "13:00", "long"), ("13:30", "long", "13:30", "long"), ("14:00", "long", "14:00", "long"), ("14:30", "long", "14:30", "long"), ("15:00", "long", "15:00", "long"), ("15:30", "long", "15:30", "long"), ("16:00", "long", "16:00", "long"), ("16:30", "long", "16:30", "long"), ("17:00", "long", "17:00", "long"), ("17:30", "long", "17:30", "long"), ("18:00", "long", "18:00", "long"), ("18:30", "long", "18:30", "long"), ("19:00", "long", "19:00", "long"), ("19:30", "long", "19:30", "long"), ("20:00", "long", "20:00", "long"), ("20:30", "long", "20:30", "long"), ("21:00", "long", "21:00", "long"), ("21:30", "long", "21:30", "long"), ("22:00", "long", "22:00", "long"), ("22:30", "long", "22:30", "long"), ("23:00", "long", "23:00", "long"), ("23:30", "long", "23:30", "long")], transformation_ctx = "applymapping1"]
## @return: applymapping1
## @inputs: [frame = datasource0]
applymapping1 = ApplyMapping.apply(frame = datasource0, mappings = [("meter", "string", "meter", "string"), ("date", "string", "date", "string"), ("0:00", "long", "0:00", "long"), ("0:30", "long", "0:30", "long"), ("1:00", "long", "1:00", "long"), ("1:30", "long", "1:30", "long"), ("2:00", "long", "2:00", "long"), ("2:30", "long", "2:30", "long"), ("3:00", "long", "3:00", "long"), ("3:30", "long", "3:30", "long"), ("4:00", "long", "4:00", "long"), ("4:30", "long", "4:30", "long"), ("5:00", "long", "5:00", "long"), ("5:30", "long", "5:30", "long"), ("6:00", "long", "6:00", "long"), ("6:30", "long", "6:30", "long"), ("7:00", "long", "7:00", "long"), ("7:30", "long", "7:30", "long"), ("8:00", "long", "8:00", "long"), ("8:30", "long", "8:30", "long"), ("9:00", "long", "9:00", "long"), ("9:30", "long", "9:30", "long"), ("10:00", "long", "10:00", "long"), ("10:30", "long", "10:30", "long"), ("11:00", "long", "11:00", "long"), ("11:30", "long", "11:30", "long"), ("12:00", "long", "12:00", "long"), ("12:30", "long", "12:30", "long"), ("13:00", "long", "13:00", "long"), ("13:30", "long", "13:30", "long"), ("14:00", "long", "14:00", "long"), ("14:30", "long", "14:30", "long"), ("15:00", "long", "15:00", "long"), ("15:30", "long", "15:30", "long"), ("16:00", "long", "16:00", "long"), ("16:30", "long", "16:30", "long"), ("17:00", "long", "17:00", "long"), ("17:30", "long", "17:30", "long"), ("18:00", "long", "18:00", "long"), ("18:30", "long", "18:30", "long"), ("19:00", "long", "19:00", "long"), ("19:30", "long", "19:30", "long"), ("20:00", "long", "20:00", "long"), ("20:30", "long", "20:30", "long"), ("21:00", "long", "21:00", "long"), ("21:30", "long", "21:30", "long"), ("22:00", "long", "22:00", "long"), ("22:30", "long", "22:30", "long"), ("23:00", "long", "23:00", "long"), ("23:30", "long", "23:30", "long")], transformation_ctx = "applymapping1")
## @type: ResolveChoice
## @args: [choice = "make_struct", transformation_ctx = "resolvechoice2"]
## @return: resolvechoice2
## @inputs: [frame = applymapping1]
resolvechoice2 = ResolveChoice.apply(frame = applymapping1, choice = "make_struct", transformation_ctx = "resolvechoice2")
 
 
# Convert to Dataframe
df = resolvechoice2.toDF()
 
col_list = df.columns
 
col_list.remove('meter')
col_list.remove('date')
 
 # Create a new Dataframe with the additional total field.
df_agg = df.withColumn('total',reduce(add,[F.col(x) for x in col_list]))
 
# Turn it back to a dynamic frame
map1 = DynamicFrame.fromDF(df_agg, glueContext, "dynamicframe")
 
 
## @type: DropNullFields
## @args: [transformation_ctx = "dropnullfields3"]
## @return: dropnullfields3
## @inputs: [frame = map1]
dropnullfields3 = DropNullFields.apply(frame = map1, transformation_ctx = "dropnullfields3")
## @type: DataSink
## @args: [connection_type = "s3", connection_options = {"path": "s3://consumption-dev-landing/consumption/output"}, format = "parquet", transformation_ctx = "datasink4"]
## @return: datasink4
## @inputs: [frame = dropnullfields3]
datasink4 = glueContext.write_dynamic_frame.from_options(frame = dropnullfields3, connection_type = "s3", connection_options = {"path": "s3://consumption-dev-landing/consumption/output"}, format = "parquet", transformation_ctx = "datasink4")
## @type: DataSink
## @args: [catalog_connection = "pg", connection_options = {"database" : "consumption_db", "dbtable" : "consumption_per_day"}, redshift_tmp_dir = args["TempDir"]]
## @return: datasink5
## @inputs: [frame = dropnullfields3]
datasink5 = glueContext.write_dynamic_frame.from_jdbc_conf(frame = dropnullfields3, catalog_connection = "pg", connection_options = {"database" : "consumption_db", "dbtable" : "consumption_per_day"}, redshift_tmp_dir = args["TempDir"])

job.commit()