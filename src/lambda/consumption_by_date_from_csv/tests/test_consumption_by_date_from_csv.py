import pytest
from unittest.mock import MagicMock
import consumption_by_date_from_csv as src

def test_invalid_date_returns_no_result():
    event = {}
    context = {}
    event['date'] = "2999-12-31"
    # Patch out the calls to S3
    result = src.lambda_handler(event,context)
    expected = {'statusCode': 200, 'body': '"Data not found"'}
    # assert result == expected
    assert False # fail until the test is written properly

def test_valid_date_returns_expected_load():
    event = {}
    context = {}
    event['date'] = "2019-01-01"
    # Patch out the calls to S3, returning the dataset with consumption = 200 for that date 
    result = src.lambda_handler(event,context)
    expected = {'statusCode': 200, 'body': '200'}
    # assert result == expected
    assert False # fail until the test is written properly