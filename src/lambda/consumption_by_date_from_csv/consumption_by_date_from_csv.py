import json
import boto3
import csv



# assuming that the Glue job would append to a single CSV that contained date, total_consumption
def lambda_handler(event, context):
    
    date = event['date']
    
    # get file from s3
    s3 = boto3.client('s3')
    bucket = 'consumption-dev-landing'
    key = 'combined_consumption.csv'
    temp_file = '/tmp/sample.csv'
    s3.download_file(bucket, key, temp_file)


    # read the file
    with open(temp_file,'r') as csvfile:
        reader = csv.DictReader(csvfile,delimiter=',')
        for row in reader:
            if date == row['date']:
                return {
                     'statusCode': 200,
                     'body': f"Total Consumption = {row['total']}"
                }


    return {
        'statusCode': 200,
        'body': json.dumps('Data not found')
    }
