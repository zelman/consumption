CONSUMPTION_TEMPLATE=consumption_stack.cf.yaml
CONSUMPTION_CONFIG=consumption.cfg
CONFIG=dev
STACK_NAME=consumption-${CONFIG}
GLUE_JOB_TEMPLATE=glue_job.cf.yaml
GLUE_STACK_NAME=glue-job-${CONFIG}
VPC_TEMPLATE=vpc_stack.cf.yaml
VPC_STACK_NAME=vpc-${CONFIG}

validate-templates:
	$(eval TEMPLATES = $(shell find . -name '*.cf.yaml' | sort | uniq))
	@for template in $(TEMPLATES); do \
		echo "--- :cloudformation: Validating $$template" && \
		aws cloudformation validate-template \
			--template-body file://./$$template \
			--output table; \
		if [ $$? -ne 0 ]; then \
			echo "^^^ Validation failed, oh no!!" && \
			exit 1; \
		fi \
	done
	@echo "^^^ +++"
	@echo "Validation Successful, good job!!"

unit-tests:
	@echo "run pytest across the code base"


deploy-vpc-stack:
	@if [ -f ./cloudformation/$(VPC_TEMPLATE) ] ; then \
		echo "--- :cloudformation: Deploying stack $(VPC_STACK_NAME)"; \
		aws cloudformation deploy \
			--no-fail-on-empty-changeset \
			--capabilities CAPABILITY_NAMED_IAM \
			--template ./cloudformation/$(VPC_TEMPLATE) \
			--stack-name $(VPC_STACK_NAME) \
			--parameter-overrides \
				Configuration=$(CONFIG) \
				$(shell cat ./config/$(CONFIG)/$(CONSUMPTION_CONFIG)); \
	else \
		echo --- :sadpanda: No infrastructure template found.; \
	fi;



deploy-consumption-stack:
	@if [ -f ./cloudformation/$(CONSUMPTION_TEMPLATE) ] ; then \
		echo "--- :cloudformation: Deploying stack $(STACK_NAME)"; \
		aws cloudformation deploy \
			--no-fail-on-empty-changeset \
			--capabilities CAPABILITY_NAMED_IAM \
			--template ./cloudformation/$(CONSUMPTION_TEMPLATE) \
			--stack-name $(STACK_NAME) \
			--parameter-overrides \
				Configuration=$(CONFIG) \
				$(shell cat ./config/$(CONFIG)/$(CONSUMPTION_CONFIG)); \
	else \
		echo --- :sadpanda: No infrastructure template found.; \
	fi;

deploy-glue-stack:
	@if [ -f ./cloudformation/$(GLUE_JOB_TEMPLATE) ] ; then \
		echo "--- :cloudformation: Deploying stack $(GLUE_STACK_NAME)"; \
		aws cloudformation deploy \
			--no-fail-on-empty-changeset \
			--capabilities CAPABILITY_NAMED_IAM \
			--template ./cloudformation/$(GLUE_JOB_TEMPLATE) \
			--stack-name $(GLUE_STACK_NAME) \
			--parameter-overrides \
				Configuration=$(CONFIG) \
				$(shell cat ./config/$(CONFIG)/$(CONSUMPTION_CONFIG)); \
	else \
		echo --- :sadpanda: No infrastructure template found.; \
	fi;

deploy-lambda-code:
	@echo "---run the deploy.sh for each lambda to upload the code"
	cd ./src/lambda/consumption_by_date_from_csv && ./deploy.sh API_Lambda

delete-vpc-stack:
	@echo "--- :cloudformation: Deleting vpc stack"; \
	aws cloudformation delete-stack --stack-name $(VPC_STACK_NAME)


delete-consumption-stack:
	@echo "--- :cloudformation: Deleting infrastructure stack"; \
	aws cloudformation delete-stack --stack-name $(STACK_NAME)

delete-glue-stack:
	@echo "--- :cloudformation: Deleting glue stack"; \
	aws cloudformation delete-stack --stack-name $(GLUE_STACK_NAME)

upload-glue-code:
	@echo "--- Uploading Glue job code to S3"; \
	aws s3 cp ./src/glue/glue_job.py s3://consumption-dev-landing/glue_scripts/glue_job.py

deploy: test deploy-vpc-stack deploy-consumption-stack upload-glue-code deploy-glue-stack deploy-lambda-code
destroy: delete-glue-stack delete-consumption-stack delete-vpc-stack
test: validate-templates unit-tests