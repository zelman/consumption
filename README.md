# Technical Test - Consumption

## Problem Statement
Given a cs file containing consumption of various energy meters.  Provide a cloud native solution  using AWS that consumes (via batch processing rather than real time streaming) a large number of these files into a data store and an API to sum total consumption per day

## Assumptions
The sample file contains one month of data, for one meter.  Let's assume that the system will receive one file per meter, every month.  Also, while the sample file is named consumption.csv, if each file is representative of a single meter, then each file will actually be named <meter_id>.csv

According to the SMH, [there were approx 13M meters in Australia in 2018](https://www.smh.com.au/business/consumer-affairs/what-s-so-smart-about-electricity-smart-meters-20180314-p4z4bb.html), so the system will expect around 13M files arriving on a single day.

The problem statement doesn't go into the specifics of the API, so I'm assuming the end user will specify a date range, and the response will have the total consumption for all meters in Australia per day across the date range.

I will also assume that the data store will not be used for any other purpose, which allows for pre-aggregation of the data to a dataset that is just date and total consumption.  In a real world scenario, there may be a need to maintain the data set at a lower level of granularity, which would then move the aggregation to query time.


## High level architecture
### Components
- VPC with public and private subnets
- API Gateway with Lambda for external access to consumption data
- Postgres Database to store the aggregated consumption data.  This will sit in the private subnet of the VPC
- Glue job and trigger to run the aggregation of the files once a month and load into the database
- S3 bucket to allow external parties to load the csvs
- Lambda to validate and potentially reject bad files.
- Lambda to archive files after processing

### Consumption
External User <--> API Gateway <--> Lambda <--> Data Store
#### Details
- The External User will post the date
- The API Gateway then triggers the Lambda, passing through the date param
- The Lambda will run the query statement to extract the result data set from the data store, and pass this back through the API Gateway to the User
- Postgres RDS is used for the data store - if Glue is doing the aggregation, then the queries on the data store will be limited to "select total from table where date=param" - Redshift will not provide any additional benefit here.  On the other end of the scale, data could be stored in files in S3, eg. hive partitioned and accessed via Athena, since the queries and dataset are so simple.  Performance would probably not be fantastic over time, and it really limits the solution to the sole use case - any further features (eg. different levels of aggregation) would probably necessitate moving to a database solution.
### Ingestion
External Source --> Transfer --> S3 --> Lambda --> S3 --> ETL Engine (Glue) --> Data Store
#### Details
- Files may be transferred by SFTP.  Potentially use the S3 SFTP solution, but is not cost effective to keep it running if files come in monthly.  Alternatively, can set up a server using EC2, but would require more moving parts.
- When files are transferred to S3, they will trigger a Lambda that will validate the file: file name/format, contents.
- Files that fail validation should be moved to a "reject" location, and notifications should be sent out
- The ETL Engine will be triggered on a monthly schedule to pick up all the files that have been delivered, do the aggregation and insert into the Data Store
- At the completion of the ETL Engine job, there should be a Lambda that moves the files to an archive location, set to Infrequent-Access

### Roles
#### API
- Have a role that limits the access to the API by having a policy that allows only to invoke the GET method.
- The API role will need to be able to invoke the Lambda
#### Lambdas
- Restrict the Validation Lambda and the Archiving Lambda to only have Get/Put/DeleteObject on the specific S3 bucket/prefixes.
#### Glue
- The Glue role should be limited to StartJobRun and any permissions required for the job execution (eg. GetConnection?)
#### S3
- Deny access to the bucket except for the roles created for Lambda/Glue/External file delivery
#### Other Access
- There should be no "human" access other than a read-only devops/support role
- The CICD tool should have a role created to access to create the required infrastructure


### CICD
A Makefile is used to semi-automate testing and deployments.  The CICD tool should be able to pick up this repo (eg. in a Docker container), and run the Make steps required to deploy into different environments/AWS accounts.  Ideally you'd separate the Account-level setup (VPCs, IAM) from the solution-level infrastructure, and also from the solution code.

### Testing
It's possible but probably overkill to do any closer testing on the CloudFormation templates beyond the AWS validate-templates functionality, with the possible exception to this being the security setup.  Lambda code is written in Python, so automated unit testing can be conducted via pytest, along with coverage checks, pylint.  Glue-specific code may not be compatible with granular unit testing, but in this scenario the input files are clean (validated in an earlier lambda), so testing is limited to ensuring the job behaves correctly for happy day scenario (1 or more files present), and for when no files are available to be loaded.

### Performance and Monitoring
CloudWatch dashboards and graphs can be used to monitor the performance of the components, eg latency on the API Gateway, duration on lambda processing, etc.  CloudTrail should be used to monitor any human access to the system.



### Explanation of what's incomplete
#### Glue connection to RDS
The Glue Connection to RDS Postgres was fine, but the job couldn't connect. I think it's to do with the subnet configuration.
#### API Lambda to RDS
Without Postgres RDS set up with data, I didn't get around to updating the API Lambda from my original POC on hitting a results csv in S3
#### API passthrough to Lambda
Almost there, but just not passing in the right variables either in the url or in the passthrough.

### Explanation of what I haven't done
#### Validation Lambda
Triggered on object create trigger in S3. Take the key from the Event, attempt to read in using CSVReader with the following checks
- Header row exists
- Row count matches number of days for the month
- File schema matches expected (meter,date,00:00....)
- Fail any validation and the file should be moved to the rejects path so that it's not picked up by Glue
Unit Test script would check that each of the above validations work with good and bad sample data - assert boto3 s3 client calls to put/delete objects were made when validation fails
#### Archiving Lambda
Move all files in the inbound path to an archive path in the same s3 bucket, and set to Infrequent Access to reduce cost.  This should be run from a CloudWatch Event Rule that would run once a month.
#### SSM/Security
Move things like Postgres user/password to Secrets Manager.

